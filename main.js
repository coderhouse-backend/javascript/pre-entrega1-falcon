function init() {

    let message = ''
    let more = true
    let total = 0

    do {
        let name = getNameValue()
        if (name === null) {
            break
        }

        let price = getPriceValue()
        if (price === null) {
            break
        }

        let quantity = getQuantityValue()
        if (quantity === null) {
            break
        }

        let subtotal = (price * quantity)
        total += subtotal

        message += `Producto: ${name}\nPrecio: ${price.toFixed(2)}\nCantidad: ${quantity}\nSubtotal: ${subtotal.toFixed(2)}\n\n`

        more = confirm('¿Desea agregar más productos?')
    } while (more)

    if (message === '') {
        message = 'No se registraron productos'
    } else {
        message += `---------------------------\nTOTAL: ${total.toFixed(2)}`
    }

    alert(message)
    console.log(message)
}

function getNameValue() {
    let attempts = 3

    while (attempts > 0) {
        let name = prompt('Ingresa el nombre')
        if (name !== null && name !== '') {
            return name
        }

        attempts--

        if (attempts == 0) {
            alert('Se superaron los intentos disponible.')
        } else {
            alert(`Ingrese un nombre válido. Intentos disponibles: ${attempts}`)
        }
    }

    return null
}

function getPriceValue() {
    let attempts = 3

    while (attempts > 0) {
        let price = parseFloat(prompt('Ingresa el precio'))
        if (typeof price === 'number' && !Number.isNaN(price)) {
            return price
        }

        attempts--

        if (attempts == 0) {
            alert('Se superaron los intentos disponible.')
        } else {
            alert(`El precio ingresado no es válido. Intentos disponibles: ${attempts}`)
        }
    }

    return null
}

function getQuantityValue() {
    let attempts = 3

    while (attempts > 0) {
        let quantity = parseInt(prompt('Ingresa la cantidad'))
        if (typeof quantity === 'number' && !Number.isNaN(quantity)) {
            return quantity
        }

        attempts--

        if (attempts == 0) {
            alert('Se superaron los intentos disponible.')
        } else {
            alert(`El valor ingresado no es correcto. Intentos disponibles: ${attempts}`)
        }
    }

    return null
}

init()
